import matplotlib.pyplot as plt
import numpy as np
import math

theta = 10
attempts = 500
maxK = 30
elements = 500


def evaluni(vec, param):
    return (np.sum(np.power(vec, param)) / len(vec) * (k + 1)) ** (1 / k)


def evalexp(vec, param):
    return (np.sum(np.power(vec, param)) / len(vec) / math.factorial(k)) ** (1 / k)


def addplot(xxs, yys):
    plt.figure()
    plt.plot(xxs, yys)
    plt.xlabel("Параметр k")
    plt.ylabel("Среднеквадратическое отклонение")


xs = range(1, maxK + 1)
ys = [0] * maxK
zs = [0] * maxK

for k in xs:
    for _ in range(attempts):
        ys[k - 1] += (evalexp(np.random.exponential(theta, elements), k) - theta) ** 2
        zs[k - 1] += (evaluni(np.random.uniform(0, theta, elements), k) - theta) ** 2
    ys[k - 1] /= attempts
    zs[k - 1] /= attempts

addplot(xs, ys)
plt.savefig("res/exp.png")
addplot(xs, zs)
plt.savefig("res/uni.png")
